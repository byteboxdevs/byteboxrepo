#include <ios>
#include <iomanip>
#include <utility>
#include <stdlib.h>
#include "includes/Graph.h"

void build_node() {
    EdgeNode *n = new EdgeNode(3);
    n->PrintNodeData();
    std::cout << "--->>--->> TestNode:: n->Key() " << n->Key()  << std::endl;
    std::cout << "--->>--->> TestNode:: n->Value() " << *(n->Value())  << std::endl;

    EdgeNode e(4);
    e.PrintNodeData();

    delete n;
}

void build_graph() {
    std::cout << "--->>--->> Graph_Util:: test "  << std::endl;
    BaseGraph *G = new BaseGraph(5);
    std::cout << G->VerticeCount() << std::endl;
    std::cout << G->EdgeCount() << std::endl;
    std::cout << "Is Directed: " << std::boolalpha << G->Directed() << std::endl;
    G->InsertVertex(4,"James");
    G->InsertVertex(4,"James");
    G->InsertVertex(2,"Jon");
    G->InsertVertex(3,"Bastian");
    G->InsertEdge(1,3,4);
    G->InsertEdge(10,8,9);
    std::cout << "G->VerticeCount: " << G->VerticeCount() << std::endl;
    std::cout << "G->EdgeCount: " << G->EdgeCount() << std::endl;
    std::vector<EdgeNode> v = G->Vertices();
    for (auto& x: v) {
        std::cout << "======= V(G->Vertices):: " << x.Key() << std::endl;
        if (x.Key() == 3 || x.Key() == 4) {
            x.IsAdjacentTo(4);
            std::vector<Edge *> ve = x.Edges();
            std::cout << "======= Vertex " <<x.Key()<<"'s EdgeList(first):: "<<ve.at(0)->Key()<< std::endl;
        }
    }

    std::vector<Edge> e = G->Edges();
    for (auto& edge: e) {
        std::cout << "======= e(G->Edges):: " << edge.Key() << " edge->v_: " << edge.v_->Key() << std::endl;
    }
    delete G;
}

// valgrind --leak-check=full --show-leak-kinds=all ./graph_test
int main(int argc, char const *argv[]) {
    build_node();
    build_graph();
    return 0;
}