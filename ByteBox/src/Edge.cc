#include "includes/Edge.h"
// int key, std::string value = "edge" EdgeNode* v = nullptr, EdgeNode* w = nullptr
//        int key_;
//         std::string value_;
//         EdgeNode* v_;
//         EdgeNode* w_;
Edge::Edge(int key, EdgeNode* v, EdgeNode* w):key_(key), v_(v), w_(w) {
    std::cout << "Edge construct " << std::endl;
}

Edge::Edge(const Edge &rhs):key_(rhs.key_),v_(rhs.v_), w_(rhs.w_) {}

Edge::~Edge() {
    v_ = nullptr;
    w_ = nullptr;
    std::cout << "Edge Destructor" << std::endl;
}

const Edge & Edge::operator=(const Edge & rhs) {
    if (this != &rhs) {
        key_ = rhs.key_;
        // value_ = rhs.value_;
        v_ = rhs.v_;
        w_ = rhs.w_;
    }
    return *this;
}

int Edge::Opposite(int vertex_key) {
    if (vertex_key == v_->Key()) {
        return w_->Key();
    }
    return v_->Key();
}

const EdgeNode* Edge::Opposite(EdgeNode& v) const {
    if (v.Key() == v_->Key()) {
        return w_;
    }
    return v_;
}

int Edge::Key() {
    return key_;
}
// const std::string& Edge::GetValue() const {
//     return value_;
// }