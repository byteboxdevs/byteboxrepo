#include "includes/Edge_Node.h"

EdgeNode::EdgeNode(int key, std::string value):key_(key), value_(value) {
    std::cout << "--->> Edge_Node:: constructor \n"
        << std::endl;
}

EdgeNode::EdgeNode(const EdgeNode & rhs) {
    key_ = rhs.key_;
    value_ = rhs.value_;
    IncidentEdges_ = rhs.IncidentEdges_;

}

EdgeNode::~EdgeNode() {
    std::cout << "Edge Node Destructor" << std::endl;
}

const EdgeNode & EdgeNode::operator=(const EdgeNode & rhs) {
    if (this != &rhs) {
        key_ = rhs.key_;
        value_ = rhs.value_;
        IncidentEdges_ = rhs.IncidentEdges_;
    }
    return *this;
}

bool EdgeNode::IsAdjacentTo(EdgeNode& v) {
    if (!IncidentEdges_.empty() && v.Key() != key_) {
        for (auto& edge: IncidentEdges_) {
            std::cout <<"--->>--adjacentloop " << std::endl;
            EdgeNode* w = edge->Opposite(v);
            int key = w->Key();
            if (key == key_) {
                std::cout <<"--->>--->> Edge_Node("<<key_<<")::IsAdjacentTo(" << v.Key() << ") :true" << std::endl;
                return true;
            }
        }
    }
    std::cout <<"--->>--->> Edge_Node("<<key_<<")::IsAdjacentTo(" << v.Key() << ") :false" << std::endl;
    return false;
}

std::vector<Edge*> EdgeNode::Edges() {
    return IncidentEdges_;
}

int EdgeNode::Degree() {
    return IncidentEdges_.size();
}

void EdgeNode::SetKey(int new_key) {
    key_ = new_key;
}

int EdgeNode::Key() {
    return key_;
}

const std::string& EdgeNode::GetValue() const {
    return value_;
}

const std::string* EdgeNode::Value() const {
    if (!value_.empty()) {
        return &value_;
    } return nullptr;
}

bool EdgeNode::AddIncidentEdge(Edge* e) {
    if (!IncidentEdges_.empty()) {
        if(std::find(IncidentEdges_.begin(), IncidentEdges_.end(), e) != IncidentEdges_.end()) {
            std::cout << "======= Edge_Node:: AddIncidentEdge Already contains edge: " << e->Key() << std::endl;
            return false;
        }
    }
    IncidentEdges_.push_back(e);
    return true;
}

bool EdgeNode::IsAdjacentTo(int vertex_id) {
    if (!IncidentEdges_.empty() && vertex_id != key_) {
        for (auto& edge: IncidentEdges_) {
            std::cout <<"--->>--adjacentloop " << std::endl;
            int opposite_key = edge->Opposite(key_);
            if (opposite_key == vertex_id) {
                std::cout <<"--->>--->> Edge_Node("<<key_<<")::IsAdjacentTo(" << vertex_id << ") :true" << std::endl;
                return true;
            }
        }
    }
    std::cout <<"--->>--->> Edge_Node("<<key_<<")::IsAdjacentTo(" << vertex_id << ") :false" << std::endl;
    return false;
}

void EdgeNode::PrintNodeData() {
    // Node::Print_Node_Data();
}