#include "includes/Node.h"
// using namespace std; //NOLINT

Node::Node(int key, std::string value):key_(key), value_(value) {}

Node::Node(const Node & rhs):key_(rhs.key_), value_(rhs.value_) {}

Node::~Node() {}

const Node & Node::operator=(const Node & rhs) {
    if (this != &rhs) {
        key_ = rhs.key_;
        value_ = rhs.value_;
    }
    return *this;
}

int Node::Key() {
    return key_;
}

const std::string* Node::Value() const {
    if (!value_.empty()) {
        return &value_;
    } return nullptr;
}

void Node::Set_Key(int new_key) {
    key_ = new_key;
}

void Node::Set_Value(const std::string &value) {
    if (!value.empty()) {
        value_ = value;
    }
}

void Node::Set_Value(std::string* value) {
    if (!value->empty()) {
        value_ = *value;
    }
}

bool Node::Has_Value() {
    return !value_.empty();
}

void Node::Clear_Value() {
    value_.clear();
}

void Node::Print_Node_Data() {
    std::cout << "\t " << key_ << ":" << value_ << std::endl;
}
