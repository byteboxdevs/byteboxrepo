#ifndef GRAPH_H
#define GRAPH_H
#include <string>
#include <iomanip>
#include <ios>
#include <stdlib.h>
#include <utility>
#include <memory>
#include <unordered_map>
#include <vector>
#include "Edge.h"
#include "Edge_Node.h"

class BaseGraph {
    // friend class adjIterator;
    public:
        BaseGraph(int num_vertices = 1, bool directed = false);
        virtual ~BaseGraph();
        int VerticeCount() const;
        int EdgeCount() const;
        bool Directed() const;
        void InsertEdge(int edge_id, int src_id, int dest_id);
        void InsertVertex(int key, std::string value = "default");
        void RemoveEdge(Edge e);
        void RemoveVertex(int key);
        const Edge& GetEdge(int start, int end);
        std::vector<EdgeNode> Vertices();
        std::vector<Edge> Edges();
        bool EdgeExists(int v, int w) const;
        int InDegree(int vertex_key);
        int OutDegree(int vertex_key);
        // void PrintAdjList();
        int NumVertices_;
        int NumEdges_;
        bool Directed_;
        std::unordered_map< int, std::shared_ptr<EdgeNode> > VertexList_;
        std::unordered_map< int, std::shared_ptr<Edge> > EdgeList_;
        // std::vector<Edge> EdgeList_;
        // std::vector<EdgeNode> VertexList_;

    private:
        bool Contains(int vertex_key);
        bool ContainsEdge(int edge_key);
        EdgeNode* GetVertex(int key);
        int Degree(int vertex_key);
        // void CleanAdjList();
//   public String toString() {
//     StringBuilder sb = new StringBuilder();
// //     sb.append("Edges:");
// //     for (Edge<E> e : edges) {
// //       Vertex<V>[] verts = endVertices(e);
// //       sb.append(String.format(" (%s->%s, %s)", verts[0].getElement(), verts[1].getElement(), e.getElement()));
// //     }
// //     sb.append("\n");
//     for (Vertex<V> v : vertices) {
//       sb.append("Vertex " + v.getElement() + "\n");
//       if (isDirected)
//         sb.append(" [outgoing]");
//       sb.append(" " + outDegree(v) + " adjacencies:");
//       for (Edge<E> e: outgoingEdges(v))
//         sb.append(String.format(" (%s, %s)", opposite(v,e).getElement(), e.getElement()));
//       sb.append("\n");
//       if (isDirected) {
//         sb.append(" [incoming]");
//         sb.append(" " + inDegree(v) + " adjacencies:");
//         for (Edge<E> e: incomingEdges(v))
//           sb.append(String.format(" (%s, %s)", opposite(v,e).getElement(), e.getElement()));
//         sb.append("\n");
//       }
//     }
//     return sb.toString();
//   }
// }
};
#endif // GRAPH_H
