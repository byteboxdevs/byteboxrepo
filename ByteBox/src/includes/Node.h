#ifndef NODE_H
#define NODE_H
// Generic Node Class (can be used in BST's graphs etc)
// First Version will be with basic Integer values as Key and Value
// Needs a Key, Value
#include <iostream>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <ios>
#include <stdlib.h>
#include <memory>
class Node {
    public:
        Node(int key = -1, std::string value = ""); //empty node
        Node(const Node &rhs);
        virtual ~Node();
        const Node & operator=(const Node & rhs);
        virtual int Key();
        const std::string* Value() const;
        void Set_Key(int new_key);
        bool Has_Value();
        void Clear_Value();
        void Set_Value(std::string* value);
        void Set_Value(const std::string &value);
        virtual void Print_Node_Data();
        //add operator << overload for easy printing
    private:
        int key_;
        std::string value_;
};
#endif // NODE_H
