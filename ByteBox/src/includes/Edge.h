#ifndef EDGE_H
#define EDGE_H
#include "Edge_Node.h"
class EdgeNode;
class Edge {
    public:
        Edge(int key, EdgeNode* v = nullptr, EdgeNode* w = nullptr);
        virtual ~Edge();
        Edge(const Edge &rhs);
        const Edge & operator=(const Edge & rhs);
        int Key();
        std::vector<EdgeNode> EndVertices();
        bool IsIncidentOn(int vertex_key);
        int Opposite(int vertex_key);
        const EdgeNode* Opposite(EdgeNode& v) const;
        // const std::string& GetValue() const;
        int key_;
        EdgeNode* v_;
        EdgeNode* w_;
};
#endif // EDGE_H
