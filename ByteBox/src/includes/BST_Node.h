#ifndef BST_NODE_H
#define BST_NODE_H
#include "Node.h"

class BST_Node : public Node {
    public:
        BST_Node(int key = -1,
                 std::string value = "",
                 int size = 0);
        BST_Node(const BST_Node &rhs);
        virtual ~BST_Node();
        const BST_Node & operator=(const BST_Node & rhs);
        virtual void Print_Node_Data();
        int Size() const;
        int* pSize();
        void Set_Size(int new_size);
        const BST_Node& Left() const;
        const BST_Node& Right() const;
        BST_Node* Left();
        BST_Node* Right();
        void Set_Left_Child(BST_Node &left);
        void Set_Left_Child(BST_Node* left = nullptr);
        void Set_Right_Child(BST_Node &right);
        void Set_Right_Child(BST_Node* right = nullptr);
        BST_Node* left_child;
        BST_Node* right_child;
        // friend std::ostream& operator<<(std::ostream &Str, const intern_val_record& v);
    private:
        int size_; //number of nodes rooted here
};
#endif // BST_NODE_H
