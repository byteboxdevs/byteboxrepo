#ifndef EDGE_NODE_H
#define EDGE_NODE_H
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <algorithm>
#include "Edge.h"
class Edge;
class EdgeNode{

    public:
        EdgeNode(int key = -1,
                 std::string value = "edgenode");
        EdgeNode(const EdgeNode &rhs);
        virtual ~EdgeNode();
        const EdgeNode & operator=(const EdgeNode & rhs);
        bool IsAdjacentTo(EdgeNode& v);
        std::vector< Edge* > Edges();
        int Degree(); //Aggregate # of Incident edges (ignores directed)
        virtual int Key();
        void SetKey(int new_key);
        const std::string& GetValue() const;
        const std::string* Value() const;
        bool AddIncidentEdge(Edge* e);
        bool RemoveIncidentEdge(Edge* e);
        bool IsAdjacentTo(int vertex_id);
        virtual void PrintNodeData();

        // operator*();
        int key_;
        std::string value_;
        std::vector< Edge* > IncidentEdges_;
};
#endif // EDGE_NODE_H
