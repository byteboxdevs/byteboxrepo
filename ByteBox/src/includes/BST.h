#ifndef BST_H
#define BST_H
#include <cassert> //NOLINT
#include <vector>
#include <stack>
#include <queue>
#include <exception>
#include <algorithm>
#include <sstream>
// #include <stdexcept> may need to include this instead
// #define NDEBUG //uncomment to disable assert
#include "BST_Node.h"

class BST {
    friend std::ostream& operator<<(std::ostream &os, const BST& tree);

    public:
        BST();
        virtual ~BST();
        bool Empty();
        BST_Node* Get_Root();
        BST_Node* Get_kRoot() const;
        bool Contains(int key);
        const std::string* Get(int key);
        void Put(int key, std::string* value);
        void Delete_Min();
        void Delete_Max();
        void Delete(int key);
        int Min_Key(BST_Node* x) const;
        int Max_Key(BST_Node* x) const;
        int Floor(int key); // find the largest key k smaller than key
        int Ceiling(int key); // find the smallest key k larger than key
        int Select(int k); // select the kth element in the table
        int Height(); // Height of BST
        int Get_Height(BST_Node* x) const;
        int Rank(int key); // # keys in table strictly less than key
        int Size();
        int Get_Size() const;
        void Print_Level_Order();
        void Print_In_Order();
        void Print_Pretty();
        bool Is_BST();
        // Keys<iterable>
        // void Create_Random();


    private:
        const std::string* Get(BST_Node* x, int key);
        BST_Node* Put(BST_Node* x, int key, std::string* value);
        BST_Node* Delete_Max(BST_Node* x);
        BST_Node* Delete_Min(BST_Node* x);
        BST_Node* Get_Successor(BST_Node* x);
        BST_Node* Delete(BST_Node* x, int key);
        BST_Node* Floor(BST_Node* x, int key);
        BST_Node* Ceiling(BST_Node* x, int key);
        BST_Node* Select(BST_Node* x, int k);
        int Height(BST_Node* x);
        int Rank(BST_Node* x, int key);
        int Size(BST_Node* x);
        void Print_In_Order(BST_Node* x);
        void Print_Pretty(BST_Node* x, std::stringstream* ss, int indent = 0);
        bool Is_BST(BST_Node* x, int min, int max);
        BST_Node* Min(BST_Node* x);
        BST_Node* Max(BST_Node* x);
        BST_Node* Root_;
};
#endif // BST_H
