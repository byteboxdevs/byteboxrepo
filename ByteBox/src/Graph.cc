#include "includes/Graph.h"

BaseGraph::BaseGraph(int num_vertices, bool directed):NumVertices_(num_vertices), NumEdges_(0), Directed_(directed) {
    std::cout << "BaseGraph Constructor";
}

BaseGraph::~BaseGraph() {
    std::cout << "BaseGraph Destructor" << std::endl;

}

int BaseGraph::VerticeCount() const {
    return VertexList_.size();
}

int BaseGraph::EdgeCount() const {
    return EdgeList_.size();
}

bool BaseGraph::Directed() const {
    return Directed_;
}

void BaseGraph::InsertEdge(int edge_id, int src_id, int dest_id) {
    if (ContainsEdge(edge_id)) {
        std::cout << "--->> Graph::InsertEdge -- edge_id: " << edge_id << " already exists!" << std::endl;
        return;
    }
    if (!Contains(src_id)) {
        InsertVertex(src_id);
    }
    if (!Contains(dest_id)) {
        InsertVertex(dest_id);
    }
    EdgeNode* source = GetVertex(src_id);
    EdgeNode* dest = GetVertex(dest_id);
    std::shared_ptr<Edge> e(new Edge(edge_id, source, dest));
    EdgeList_.insert(std::make_pair(edge_id, e));
    source->AddIncidentEdge(e.get());
    if (!Directed()) dest->AddIncidentEdge(e.get());
}

void BaseGraph::InsertVertex(int key, std::string value) {
    if (Contains(key)) {
        std::cout << "--->> Graph::InsertVertex -- key: " << key << " already exists!" << std::endl;
        return;
    } else {
        std::shared_ptr<EdgeNode> v(new EdgeNode(key, value));
        VertexList_.insert(std::make_pair(key, v));

        for (auto& x: VertexList_)
            std::cout << "--->> Graph:: iterate after insert: " << x.first
                <<" EdgeNode->Key(): " << x.second->Key() << std::endl;
    }
}

void BaseGraph::RemoveEdge(Edge e) {
    // int v = e.v_; //->Key();
    // int w = e.w_; //->Key();
    // EdgeNode *v_tmp;
    // EdgeNode *w_tmp;
        // remove this edge from vertices' adjacencies
    // InnerVertex<V>[] verts = (InnerVertex<V>[]) edge.getEndpoints();
    // verts[0].getOutgoing().remove(verts[1]);
    // verts[1].getIncoming().remove(verts[0]);
}

void BaseGraph::RemoveVertex(int key) {
    //remove associated edges as well
}

bool BaseGraph::EdgeExists(int v, int w) const {
    //if edge with source v dest w exists return true
    //if undirected, the vertices are interchangeable
    return true;
}

std::vector<EdgeNode> BaseGraph::Vertices() {
    std::vector<EdgeNode> vertices;
    vertices.reserve(VertexList_.size());
    for (auto& x: VertexList_)
        vertices.push_back(*(x.second.get()));
    return vertices;
}

std::vector<Edge> BaseGraph::Edges() {
    std::vector<Edge> edges;
    edges.reserve(EdgeList_.size());
    for (auto& x: EdgeList_)
        edges.push_back(*(x.second.get()));
    return edges;
}

bool BaseGraph::Contains(int vertex_key) {
    if (VertexList_.empty()) return false;
    std::unordered_map< int, std::shared_ptr<EdgeNode> >::const_iterator vertexItor =  VertexList_.find(vertex_key);
    return vertexItor != VertexList_.end();
}

bool BaseGraph::ContainsEdge(int edge_key) {
    if (EdgeList_.empty()) return false;
    std::unordered_map< int, std::shared_ptr<Edge> >::const_iterator edgeItor =  EdgeList_.find(edge_key);
    return edgeItor != EdgeList_.end();
}

EdgeNode* BaseGraph::GetVertex(int vertex_key) {
    std::unordered_map< int, std::shared_ptr<EdgeNode> >::const_iterator vertexItor =  VertexList_.find(vertex_key);
    if (vertexItor != VertexList_.end()) {
        return vertexItor->second.get();
    } else {
        return nullptr;
    }
}

int BaseGraph::Degree(int vertex_key) {
    if (Contains(vertex_key)) {
        EdgeNode* v = GetVertex(vertex_key);
        return v->Degree();
    } return -1;
}
// void BaseGraph::PrintAdjList() {

//     // for (auto * element : AdjList_) {
//     //     std::cout <<"- " << element->Key() << std::endl;
//     // }

// }