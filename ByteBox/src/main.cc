#include <ios>
#include <iomanip>
#include <utility>
#include <stdlib.h>
#include "includes/Graph.h"
#include "includes/BST_Node.h"
#include "includes/BST.h"
// #include <syslog.h>
#include <sstream>
using namespace std; //NOLINT

void test_printing() {
    std::string x1 = "James";
    std::string x2 = "Jon";
    std::string x3 = "Dave";
    std::string x4 = "Jacob";
    std::string x5 = "jack";
    std::string x6 = "jeb";
    BST *tree = new BST();
    tree->Put(5, &x1);
    tree->Put(3, &x3);
    tree->Put(4, &x4);
    tree->Put(2, &x5);
    tree->Put(7, &x5);
    tree->Put(6, &x6);
    tree->Put(10, &x2);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Print_Level_Order();
    std::cout << std::endl;
    tree->Print_Pretty();
    std::cout << "--->>--->> tree output stream:: " << *tree  << std::endl;
    delete tree;
}

void test_del_min() {
    std::string x1 = "James";
    std::string x2 = "Jon";
    std::string x3 = "Dave";
    std::string x4 = "Jacob";
    BST *tree = new BST();
    tree->Put(5, &x4);
    tree->Put(2, &x2);
    tree->Put(4, &x3);
    tree->Put(1, &x1);
    tree->Put(15, &x1);
    tree->Put(10, &x1);
    tree->Put(9, &x1);
    tree->Put(8, &x3);
    tree->Put(7, &x3);
    tree->Print_Level_Order();
    tree->Print_In_Order();
    int del_ctr = 0;
    while (!tree->Empty()) {
        std::cout << std::endl;
        tree->Print_In_Order();
        tree->Delete_Max(); // test with delete_min as well
        del_ctr++;
        std::cout << std::endl;
    }
    std::cout << "======= test_del_min:: del_ctr = " << del_ctr << std::endl;
    tree->Print_In_Order();
    delete tree;
}

void small_tree_delete() {
    std::string x1 = "James";
    std::string x2 = "Jon";
    std::string x3 = "Dave";
    BST *tree = new BST();
    tree->Put(3, &x1);
    tree->Put(4, &x2);
    tree->Put(2, &x3);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Delete(3);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Delete(4);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Delete(2);
    tree->Print_In_Order();
    std::cout << std::endl;
    delete tree;
}

void test_basic_delete(int delkey) {
    // Create Tree with Nodes: 5,3,4,2,7,6,10
    // In order traversal: 2,3,4,5,6,7,10
    std::string x1 = "James";
    std::string x2 = "Jon";
    std::string x3 = "Dave";
    std::string x4 = "Jacob";
    std::string x5 = "jack";
    std::string x6 = "jeb";
    std::string x7 = "Bastian";
    BST *tree = new BST();
    tree->Put(5, &x1);
    tree->Put(3, &x3);
    tree->Put(4, &x4);
    tree->Put(2, &x5);
    tree->Put(7, &x5);
    tree->Put(6, &x6);
    tree->Put(10, &x2);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Put(10, &x7);
    tree->Print_In_Order();
    std::cout << std::endl;
    tree->Delete(delkey);
    tree->Print_In_Order();
    std::cout << std::endl;
    delete tree;
}

void test_bst() {
    std::string x1 = "James";
    std::string x2 = "Jon";
    std::string x3 = "Dave";
    std::string x4 = "Jacob";
    std::string x5 = "jack";
    BST *tree = new BST();
    std::cout << "--->>--->> main:: Tree->Height() " << tree->Height() << std::endl;
    tree->Put(5, &x1);
    std::cout << "--->>--->> main:: Tree->Height() " << tree->Height() << std::endl;
    tree->Put(2, &x3);
    tree->Put(3, &x4);
    std::cout << "--->>--->> main:: Tree->Height() " << tree->Height() << std::endl;
    tree->Put(1, &x5);
    tree->Put(10, &x5);
    tree->Put(8, &x5);
    tree->Put(7, &x5);
    tree->Put(9, &x5);
    tree->Put(18, &x5);
    tree->Put(17, &x5);
    tree->Put(19, &x5);
    tree->Print_In_Order();
    std::cout << "--->>--->> Tree->Size():: " << tree->Size() << std::endl;
    tree->Delete(2);
    std::cout << "--->>--->> Tree->Size() after-delete:: " << tree->Size() << std::endl;
    std::cout << "Get(2): " << std::boolalpha << tree->Contains(2) << std::endl;
    tree->Print_In_Order();
    std::cout << "--->>--->> main:: Tree->Floor(6) " << tree->Floor(6)  << std::endl;
    std::cout << "--->>--->> main:: Tree->Ceiling(6) " << tree->Ceiling(6)  << std::endl;
    std::cout << "--->>--->> main:: Tree->Select(3) " << tree->Select(3)  << std::endl;
    std::cout << "--->>--->> main:: Tree->Rank(10) " << tree->Rank(10)  << std::endl;
    std::cout << "--->>--->> main:: Tree->Height() " << tree->Height() << std::endl;
    tree->Delete_Max();
    std::cout << "--->>--->> Tree->Size()::Delete_Max " << tree->Size() << std::endl;
    tree->Print_In_Order();
    delete tree;
}

void test_bst_node() {
    // BST_Node *n = new BST_Node(2,"JamesD",1);
    // std::cout<<"N->Size(): "<<n->Size()<<std::endl;
    // // BST_Node n_left(1,"Jon",1);
    // // BST_Node n_right(3,"Dave",1);
    // n->left_child = new BST_Node(1,"Jon",1);
    // n->right_child = new BST_Node(3,"Dave",1);
    // n->Set_Left_Child(&n_left);
    // n->Set_Right_Child(&n_right);
    // n->Set_Size(3);
    // std::cout<<"N->Size(): "<<n->Size()<<std::endl;
    // BST_Node nnleft(5,"jack",1);
    // n->Left()->Set_Left_Child(&nnleft);
    // n->Print_Node_Data();
    // delete n;
}

///////////////////////////////////////////////////////////////////////////
// NOTE: GCC version 5+ and Valgrind do not play nice,
// valgrind reports a leak of 72,704 bytes in 1 blocks (still reachable)
// This is not an issue with the current C program, but with GCC and Valgrind
// It is actively being worked on
// valgrind --leak-check=full --show-leak-kinds=all ./basic_graph
int main(int argc, char const *argv[]) {
    std::cout <<"--->>--->> main::Hello ByteBox " << std::endl;
    test_bst();
    test_basic_delete(3);
    small_tree_delete();
    test_printing();
    test_basic_delete(10);
    test_basic_delete(2);
    test_basic_delete(5);
    test_del_min();
    test_printing();
    return 0;
}
