#include "includes/BST_Node.h"

BST_Node::BST_Node(int Key, std::string value, int size):Node(Key, value), size_(size) {
    left_child = nullptr;
    right_child = nullptr;
}

BST_Node::BST_Node(const BST_Node & rhs):Node(rhs), size_(rhs.size_) {
    left_child = new BST_Node(*rhs.left_child);
    right_child = new BST_Node(*rhs.right_child);
}

BST_Node::~BST_Node() {
    delete left_child;
    delete right_child;
}

const BST_Node & BST_Node::operator=(const BST_Node & rhs) {
    if (this != &rhs) {
        Node::operator=(rhs);
        size_ = rhs.size_;
        *left_child = *rhs.left_child;
        *right_child = *rhs.right_child;
    }
    return *this;
}

void BST_Node::Print_Node_Data() {
    Node::Print_Node_Data();
    std::cout << "\t /  \t\\" << std::endl;
    if (left_child != nullptr) {
        std::string t = *(left_child->Value());
        std::cout << left_child->Key() << ":" << t << " \t ";
    } else {
        std::cout <<"(nullptr) \t ";
    }
    if (right_child != nullptr) {
        std::string t = *(right_child->Value());
        std::cout << right_child->Key() << ":" << t << std::endl;
    } else {
        std::cout << "(nullptr)" << std::endl;
    }
    std::cout << "\n---end---" << std::endl;
}

int BST_Node::Size() const {
    return size_;
}

int* BST_Node::pSize() {
    return &size_;
}

void BST_Node::Set_Size(int new_size) {
    size_ = new_size;
}

const BST_Node& BST_Node::Left() const {
    return *left_child;
}

const BST_Node& BST_Node::Right() const {
    return *right_child;
}

BST_Node* BST_Node::Left() {
    return left_child;
}

BST_Node* BST_Node::Right() {
    return right_child;
}

void BST_Node::Set_Left_Child(BST_Node &left) {
    std::cout << "Set_Left_Child" << std::endl;
    *left_child = left;
}

void BST_Node::Set_Left_Child(BST_Node* left) {
    std::cout << "Set_Left_Child" << std::endl;
    left_child = left;
}

void BST_Node::Set_Right_Child(BST_Node &right) {
    *right_child = right;
    std::cout << "Set_Right_Child" << std::endl;
}

void BST_Node::Set_Right_Child(BST_Node* right) {
    std::cout << "Set_Left_Child" << std::endl;
    right_child = right;
}

// std::ostream& operator<<(std::ostream& Str, const intern_val_record& v) {
//    std::string Value = (v.has_value_) ? (*v.value_.get()):("<no value>");

//    std::string Version = (v.has_version_) ? (*v.version_.get()):("<noversion>");

//    std::string Tag = (v.has_tag_) ? (*v.tag_.get()):("<notag>");


//   Str<<"-------------------------------"<<
//   "\n Value: "<<Value<<
//   ",\n Version: "<<Version<<
//   ",\n Tag: "<<Tag<<
//   ",\n algorithm: "<<int(v.algorithm_);
//   std::string record = (v.is_marshalled_) ? (*v.marshalled_value_.get()):("<not marshalled>");
//   Str<<",\n Record: "<<record<<
//   "\n-------------------------------"<<
//   "\n-------------------------------"<<endl;
//   return Str;
// }
