#include "includes/BST.h"
using namespace std; //NOLINT

BST::BST():Root_(nullptr) {}

BST::~BST() {
    delete Root_;
}

bool BST::Empty() {
    return Root_ == nullptr;
}

BST_Node* BST::Get_Root() {
    return Root_;
}

BST_Node* BST::Get_kRoot() const {
    return Root_;
}

bool BST::Contains(int key) {
    if (!key) return false;
    return Get(key) != nullptr;
}

const std::string* BST::Get(int key) {
    return Get(Root_, key);
}

void BST::Put(int key, std::string* value) {
    if (!key || key < 0) throw std::runtime_error("Put() cannot use negative or empty Key");
    if (!value || value->empty()) throw std::runtime_error("Put() cannot use empty Value");
    Root_ = Put(Root_, key, value);
}

void BST::Delete_Max() {
    if (Empty()) throw std::runtime_error("Symbol Table Underflow");
    Root_ = Delete_Max(Root_);
    assert(Is_BST()); //NOLINT
}

void BST::Delete_Min() {
    if (Empty()) throw std::runtime_error("Symbol Table Underflow");
    Root_ = Delete_Min(Root_);
    assert(Is_BST()); //NOLINT
}

void BST::Delete(int key) {
    if (!key || key < 0) throw std::runtime_error("Delete() cannot use negative or empty Key");
    if (Empty()) {
        std::cerr << "Error: Table is Empty" << std::endl;
        return;
    }
    Root_ = Delete(Root_, key);
    assert(Is_BST()); //NOLINT
}

int BST::Min_Key(BST_Node* x) const {
    if (x->left_child == nullptr) return x->Key();
    return Min_Key(x->left_child);
}

int BST::Max_Key(BST_Node* x) const {
    if (x->right_child == nullptr) return x->Key();
    return Max_Key(x->right_child);
}

int BST::Floor(int key) {
    if (Empty()) throw std::runtime_error("Floor(): The Symbol Table is Empty");
    if (!key || key < 0) throw std::runtime_error("Floor() cannot use negative or empty Key");
    BST_Node *x = Floor(Root_, key);
    if (x == nullptr) return -1;
    return x->Key();
}

int BST::Ceiling(int key) {
    if (Empty()) throw std::runtime_error("Ceiling(): The Symbol Table is Empty");
    if (!key || key < 0) throw std::runtime_error("Ceiling() cannot use negative or empty Key");
    BST_Node *x = Ceiling(Root_, key);
    if (x == nullptr) return -1;
    return x->Key();
}

int BST::Select(int k) {
    if (k < 0 || k >= Size()) throw std::runtime_error("Select: k must be gt 0 & lt Size");
    Node *x = Select(Root_, k);
    if (x == nullptr) return -1;
    return x->Key();
}

int BST::Height() {
    return Height(Root_);
}

int BST::Get_Height(BST_Node* x) const {
    if (x == nullptr) return -1;
    return 1 + std::max(Get_Height(x->left_child) , Get_Height(x->right_child));
}

int BST::Rank(int key) {
    if (!key || key < 0) throw std::runtime_error("Rank() cannot use negative or empty Key");
    if (Empty()) return 0;
    return Rank(Root_, key);
}

int BST::Size() {
    return Size(Root_);
}

int BST::Get_Size() const {
    if (Root_ == nullptr) return 0;
    return Root_->Size();
}

void BST::Print_Level_Order() {
    if (Empty()) { std::cout << "(empty)" <<std::endl; return; }
    std::cout << "======= BST:: Print_Level_Order =======" << std::endl;
    std::queue<BST_Node*> q;
    q.push(Root_);
    while (!q.empty()) {
        BST_Node *x = q.front();
        q.pop();
        if (x == nullptr) continue;
        std::cout << "--> " << x->Key() << std::endl;
        if (x->left_child != nullptr) q.push(x->left_child);
        if (x->right_child != nullptr) q.push(x->right_child);
    }
}

void BST::Print_In_Order() {
    Print_In_Order(Root_);
}

void BST::Print_Pretty() {
    if (Empty()) { std::cout << "(empty)" <<std::endl; return; }
    std::stringstream ss(std::ios_base::out | std::ios_base::ate);
    int border_len = Height() * 10;
    ss << "--- Print Pretty BST ---\n";
    ss << "-Size: " << Size() << " -Height: " << Height();
    ss << "-Root: " << Root_->Key() << "\n";
    ss << std::string(border_len, '-') << "\n";
    Print_Pretty(Root_, &ss);
    ss << "\n" << std::string(border_len, '-') << "\n";
    std::cout << ss.str() << std::endl;
}

bool BST::Is_BST() {
    return Is_BST(Root_, -1, -1);
}

const std::string* BST::Get(BST_Node* x, int key) {
    if (x == nullptr) return nullptr;
    if (key < x->Key()) return Get(x->left_child, key);
    else if (key > x->Key()) return Get(x->right_child, key);
    else {
        return x->Value();
    }
}

BST_Node* BST::Put(BST_Node* x, int key, std::string* value) {
    if (x == nullptr) return new BST_Node(key, *value, 1);
    if (key < x->Key()) {
        x->left_child = Put(x->left_child, key, value);
    } else if (key > x->Key()) {
        x->right_child = Put(x->right_child, key, value);
    } else {
        x->Set_Value(value);
    }
    x->Set_Size(1 + Size(x->left_child) + Size(x->right_child));
    return x;
}

BST_Node* BST::Delete_Max(BST_Node* x) {
    BST_Node *Temp;
    if (x->right_child == nullptr) {
        Temp = x->left_child;
        x->left_child = nullptr;
        delete x;
        return Temp;
    }
    x->right_child = Delete_Max(x->right_child);
    x->Set_Size(Size(x->right_child) + Size(x->left_child) + 1);
    return x;
}

BST_Node* BST::Delete_Min(BST_Node* x) {
    BST_Node *Temp;
    if (x->left_child == nullptr) {
        Temp = x->right_child;
        x->right_child = nullptr;
        delete x;
        return Temp;
    }
    x->left_child = Delete_Min(x->left_child);
    x->Set_Size(Size(x->left_child) + Size(x->right_child) + 1);
    return x;
}

BST_Node* BST::Get_Successor(BST_Node* x) {
    if (x->left_child == nullptr) return x->right_child;
    x->left_child = Get_Successor(x->left_child);
    x->Set_Size(Size(x->left_child) + Size(x->right_child) + 1);
    return x;
}

BST_Node* BST::Delete(BST_Node* x, int key) {
    BST_Node *Temp;
    if (x == nullptr) return nullptr;
    if (key < x->Key()) {
        x->left_child = Delete(x->left_child, key);
    } else if (key > x->Key()) {
        x->right_child = Delete(x->right_child, key);
    } else {
        if (x->right_child == nullptr) {
            Temp = x->left_child;
            x->left_child = nullptr;
            delete x;
            return Temp;
        }
        if (x->left_child == nullptr) {
            Temp = x->right_child;
            x->right_child = nullptr;
            delete x;
            return Temp;
        }
        // Both Children Exist
        Temp = x;
        x = Min(Temp->right_child);
        x->right_child = Get_Successor(Temp->right_child);
        x->left_child = Temp->left_child;
        Temp->right_child = nullptr;
        Temp->left_child = nullptr;
        delete Temp;
    }
    x->Set_Size(Size(x->left_child) + Size(x->right_child) + 1);
    return x;
}

BST_Node* BST::Floor(BST_Node* x, int key) {
    if (x == nullptr) return nullptr;
    if (key == x->Key()) return x;
    if (key < x->Key()) return Floor(x->left_child, key);
    BST_Node *temp = Floor(x->right_child, key);
    if (temp != nullptr) return temp;
    else { return x; }
}

BST_Node* BST::Ceiling(BST_Node* x, int key) {
    if (x == nullptr) return nullptr;
    if (key == x->Key()) return x;
    if (key < x->Key()) {
        BST_Node *temp = Ceiling(x->left_child, key);
        if (temp != nullptr) return temp;
        else return x;
    }
    return Ceiling(x->right_child, key);
}

BST_Node* BST::Select(BST_Node* x, int k) {
    if (x == nullptr) return nullptr;
    int tmp_lsize = Size(x->left_child);
    if (tmp_lsize > k) return Select(x->left_child, k);
    else if (tmp_lsize < k) return Select(x->right_child, k - tmp_lsize - 1); //comment this
    else return x;
}

int BST::Height(BST_Node* x) {
    if (x == nullptr) return -1;
    return 1 + std::max(Height(x->left_child) , Height(x->right_child));
}

int BST::Rank(BST_Node* x, int key) {
    if (x == nullptr) return 0;
    if (key < x->Key()) return Rank(x->left_child, key);
    else if (key > x->Key()) return 1 + Size(x->left_child) + Rank(x->right_child, key);
    else return Size(x->left_child);
}

int BST::Size(BST_Node* x) {
    if (x == nullptr) return 0;
    return x->Size();
}

void BST::Print_In_Order(BST_Node* x) {
    if (x == nullptr) return;
    Print_In_Order(x->left_child);
    std::cout << "-" << x->Key() << ":" << *(x->Value()) << std::endl;
    Print_In_Order(x->right_child);
}

void BST::Print_Pretty(BST_Node* x, std::stringstream* ss, int indent) {
    if (x->right_child != nullptr) Print_Pretty(x->right_child, ss, indent + 4);
    *ss << "\n" << std::string(indent, ' ') << "(" << x->Key() << ")";
    if (x->left_child != nullptr) Print_Pretty(x->left_child, ss, indent + 4);
}

bool BST::Is_BST(BST_Node* x, int min, int max) {
    if (x == nullptr) return true;
    if ((min != -1) && (x->Key() <= min)) return false;
    if ((max != -1) && (x->Key() >= max)) return false;
    std::cout <<"--->>--->> BST::Is_BST Check " << std::endl;
    return Is_BST(x->left_child, min, x->Key()) && Is_BST(x->right_child, x->Key(), max);
}

BST_Node* BST::Min(BST_Node* x) {
    if (x->left_child == nullptr) return x;
    return Min(x->left_child);
}

BST_Node* BST::Max(BST_Node* x) {
    if (x->right_child == nullptr) return x;
    return Max(x->right_child);
}

std::ostream& operator<<(std::ostream &os, const BST& tree) {
    BST_Node *tRoot = tree.Get_kRoot();
    if (tRoot == nullptr) {
        os << "(empty)";
        return os;
    }
    std::stringstream ss(std::ios_base::out | std::ios_base::ate);
    int border_len = tree.Get_Height(tRoot) * 10;
    int tree_size = tree.Get_Size();
    int tree_root_key = tRoot->Key();
    int min_key = tree.Min_Key(tRoot);
    int max_key = tree.Max_Key(tRoot);
    ss << "\n" << std::string(border_len, '-') << "\n";
    ss << "-Size: " << tree_size << " -Height: " << tree.Get_Height(tRoot);
    ss << " -Root: " << tree_root_key << " -Min_Key: " << min_key << " -max_key: " << max_key;
    ss << "\n" << std::string(border_len, '-') << "\n";
    os << ss.str();
    return os;
}

